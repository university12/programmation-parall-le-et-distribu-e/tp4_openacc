.PHONY: build clear

build :
	mkdir -p build
	cd build && cmake ./.. && make
debug-build :
	mkdir -p debug
	cd debug && cmake -DCMAKE_BUILD_TYPE=Debug ./.. && make
clear:
	rm -rf ./build
run:
	./build/tp4
