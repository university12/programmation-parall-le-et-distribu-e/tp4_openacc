//
//  main.cpp
//

#include "Chrono.hpp"
#include "Matrix.hpp"

#include <cstdlib>
#include <ctime>
#include <iostream>
#include <stdexcept>

using namespace std;
void swapRow(double matrx1D[], double matrx1D_2[], size_t size, size_t origin,
             size_t destination, double el_val) {
#pragma acc parallel loop gang copyin(size, origin, destination, el_val)
  for (size_t i = 0; i < size; i++) {
    double temp_dest = matrx1D[destination * size + i] / el_val;
    double temp_ori = matrx1D[origin * size + i];
    matrx1D[origin * size + i] = temp_dest;
    matrx1D[destination * size + i] = temp_ori;

    double temp_dest_2 = matrx1D_2[destination * size + i] / el_val;
    double temp_ori_2 = matrx1D_2[origin * size + i];
    matrx1D_2[origin * size + i] = temp_dest_2;
    matrx1D_2[destination * size + i] = temp_ori_2;
  }
}
void initializeCMatrix(double **matrix, int size);
void to1DMatrix(double **matrix, double *matrix1D, size_t size);
void printCMatrix(double **matrix, int n) {
  size_t i, j;

  for (i = 0; i < n; i++) {
    for (j = 0; j < n; j++) {
      printf("%f ", matrix[i][j]);
    }
    printf("\n");
  }
}
void printCMatrix1D(double *matrix, int n) {
  size_t i;

  for (i = 0; i < n; i++) {
    printf("%f ", matrix[i]);
  }
  printf("\n");
}
void to1DMatrix(double **matrix, double *matrix1D, size_t size) {
  size_t c = 0;
  for (size_t i = 0; i < size; i++) {
    for (size_t j = 0; j < size; j++) {
      matrix1D[c] = matrix[i][j];
      c++;
    }
  }
}

void invertParallele(double A_1d[], double I_1d[], Matrix &iA) {

  // vérifier que la matrice est carrée
  assert(iA.rows() == iA.cols());
  // construire la matrice [A I]

  const size_t n = iA.rows();

// traiter chaque rangée
#pragma acc data copy(A_1d, I_1d) copyin(n)
  {
    for (size_t k = 0; k < n; ++k) {
      // trouver l'index p du plus grand pivot de la colonne k en valeur absolue
      // (pour une meilleure stabilité numérique).
      size_t p = k;

      double lMax = A_1d[k * n + k];
#pragma acc parallel loop reduction(max : lMax) present(p)
      for (size_t i = k; i < n; ++i) {
        if (A_1d[i * n + k] > lMax) {
          lMax = A_1d[i * n + k];
          p = i;
        }
      }
#pragma acc update device(p)

      // échanger la ligne courante avec celle du pivot
      if (p != k) {
        swapRow(A_1d, I_1d, n, k, p, lMax);
      } else {
#pragma parallele acc gang copyin(lMax)
        for (size_t j = 0; j < n; ++j) {
          // On divise les éléments de la rangée k
          // par la valeur du pivot.
          // Ainsi, lAI(k,k) deviendra égal à 1.
          A_1d[k * n + j] /= lMax; // lAI(k, j) /= lValue;
          I_1d[k * n + j] /= lMax;
        }
      }
// Pour chaque rangée...
#pragma acc kernel loop gang(1,n)
      for (size_t i = 0; i < n; ++i) {
        if (i != k) { // ...différente de k
                      // On soustrait la rangée k
                      // multipliée par l'élément k de la rangée courante
                      // lAI(i, k);
          double lValue = A_1d[i * n + k];
#pragma parallele acc loop gang(1,n) present(lValue)
          for (size_t j = 0; j < n; j++) {
            A_1d[i * n + j] -= A_1d[k * n + j] * lValue;
            I_1d[i * n + j] -= I_1d[k * n + j] * lValue;
          }
        }
      }
    }

// On copie la partie droite de la matrice AI ainsi transformée
// dans la matrice courante (this).
#pragma acc parallel loop collapse(2) gang
    for (long i = 0; i < n; ++i) {
      for (long j = 0; j < n; ++j) {
        iA(i, j) = I_1d[i * n + j];
      }
    }
  }
}
void invertSequential(Matrix &iA) {

  // vérifier que la matrice est carrée
  assert(iA.rows() == iA.cols());
  // construire la matrice [A I]
  MatrixConcatCols lAI(iA, MatrixIdentity(iA.rows()));

  // traiter chaque rangée
  for (size_t k = 0; k < iA.rows(); ++k) {
    // trouver l'index p du plus grand pivot de la colonne k en valeur absolue
    // (pour une meilleure stabilité numérique).
    size_t p = k;
    double lMax = fabs(lAI(k, k));
    for (size_t i = k; i < lAI.rows(); ++i) {
      if (fabs(lAI(i, k)) > lMax) {
        lMax = fabs(lAI(i, k));
        p = i;
      }
    }
    // vérifier que la matrice n'est pas singulière
    if (lAI(p, k) == 0)
      throw runtime_error("Matrix not invertible");

    // échanger la ligne courante avec celle du pivot
    if (p != k)
      lAI.swapRows(p, k);

    double lValue = lAI(k, k);
    for (size_t j = 0; j < lAI.cols(); ++j) {
      // On divise les éléments de la rangée k
      // par la valeur du pivot.
      // Ainsi, lAI(k,k) deviendra égal à 1.
      lAI(k, j) /= lValue;
    }

    // Pour chaque rangée...
    for (size_t i = 0; i < lAI.rows(); ++i) {
      if (i != k) { // ...différente de k
        // On soustrait la rangée k
        // multipliée par l'élément k de la rangée courante
        double lValue = lAI(i, k);
        lAI.getRowSlice(i) -= lAI.getRowCopy(k) * lValue;
      }
    }
  }
  // On copie la partie droite de la matrice AI ainsi transformée
  // dans la matrice courante (this).
  for (unsigned int i = 0; i < iA.rows(); ++i) {
    iA.getRowSlice(i) =
        lAI.getDataArray()[slice(i * lAI.cols() + iA.cols(), iA.cols(), 1)];
  }
}

// Multiplier deux matrices.
Matrix multiplyMatrix(const Matrix &iMat1, const Matrix &iMat2) {

  // vérifier la compatibilité des matrices
  assert(iMat1.cols() == iMat2.rows());
  // effectuer le produit matriciel
  Matrix lRes(iMat1.rows(), iMat2.cols());
  // traiter chaque rangée
  for (size_t i = 0; i < lRes.rows(); ++i) {
    // traiter chaque colonne
    for (size_t j = 0; j < lRes.cols(); ++j) {
      lRes(i, j) = (iMat1.getRowCopy(i) * iMat2.getColumnCopy(j)).sum();
    }
  }
  return lRes;
}

int main(int argc, char **argv) {

  srand((unsigned)time(NULL));

  unsigned int lS = 500;
  if (argc == 2) {
    lS = atoi(argv[1]);
  }

  MatrixRandom lA(lS, lS);
  // cout << "Matrice random:\n" << lA.str() << endl;

  Matrix lB(lA);
  Matrix lC(lA);
  Chrono crono;
  Matrix matrix_i = MatrixIdentity(lS);
  double A_1d[lB.cols() * lB.cols()];
  double I_1d[lB.cols() * lB.cols()];
  lB.toCMatrix(A_1d, lB.rows());
  matrix_i.toCMatrix(I_1d, lB.rows());
  invertParallele(A_1d, I_1d, lB);
  crono.pause();

  Matrix lRes = multiplyMatrix(lA, lB);
  cout << "en parallele" << endl;
  cout << "Erreur: " << lRes.getDataArray().sum() - lS << endl;
  cout << "temps: " << crono.get() << " s" << endl;

  Chrono crono2;
  invertSequential(lC);
  crono2.pause();

  Matrix lRes2 = multiplyMatrix(lA, lC);
  cout << "en sequenciel" << endl;
  cout << "Erreur: " << lRes2.getDataArray().sum() - lS << endl;
  cout << "temps: " << crono2.get() << " s" << endl;

  return 0;
}
